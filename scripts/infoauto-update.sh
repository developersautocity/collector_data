#!/bin/bash

cd /var/www/collector_data

echo 'executing infoauto:create ..'
sudo php artisan infoauto:create

echo 'executing infoauto:update ..'
sudo php artisan infoauto:update

echo 'executing infoauto:update:mongodb ..'
sudo php artisan infoauto:update:mongodb

echo 'execution done ..'
