<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoautoExtrad2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infoauto_extrad2', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ex2_codia');
            $table->string('ex2_clima');
            $table->string('ex2_fanti');
            $table->string('ex2_tcorr');
            $table->string('ex2_sesta');
            $table->string('ex2_alate');
            $table->string('ex2_acabe');
            $table->string('ex2_acort');
            $table->string('ex2_arodi');
            $table->string('ex2_isofi');
            $table->string('ex2_ctrac');
            $table->string('ex2_cesta');
            $table->string('ex2_cdesc');
            $table->string('ex2_sapen');
            $table->string('ex2_cdina');
            $table->string('ex2_bdife');
            $table->string('ex2_relef');
            $table->string('ex2_afree');
            $table->string('ex2_rparf');
            $table->string('ex2_largo');
            $table->string('ex2_ancho');
            $table->string('ex2_alto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infoauto_extrad2');
    }
}
