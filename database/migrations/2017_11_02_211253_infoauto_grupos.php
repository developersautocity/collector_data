<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoautoGrupos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infoauto_grupos', function (Blueprint $table) {
            $table->integer('gru_nmarc');
            $table->integer('gru_cgrup');
            $table->string('gru_ngrup', 30);
            $table->primary(['gru_nmarc','gru_cgrup']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('infoauto_grupos');
    }
}
