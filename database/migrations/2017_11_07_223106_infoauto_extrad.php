<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoautoExtrad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infoauto_extrad', function (Blueprint $table) {
            $table->increments('id');
            $table->stringcle('ext_codia');
            $table->string('ext_combu');
            $table->string('ext_alime');
            $table->string('ext_motor');
            $table->string('ext_puert');
            $table->string('ext_clasi');
            $table->string('ext_cabin');
            $table->string('ext_carga');
            $table->integer('ext_pesot');
            $table->integer('ext_veloc');
            $table->integer('ext_poten');
            $table->string('ext_direc');
            $table->string('ext_airea');
            $table->string('ext_tracc');
            $table->string('ext_impor');
            $table->string('ext_cajav');
            $table->string('ext_frabs');
            $table->string('ext_airba');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('infoauto_extrad');
    }
}
