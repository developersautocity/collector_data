<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoautoVersions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infoauto_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id');
            $table->integer('codigo_infoauto');
            $table->integer('codigo_reasignado');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infoauto_versions');
    }
}
