<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoautoTautos30 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infoauto_tautos30', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ta3_nmarc', 3);
            $table->string('ta3_marca', 15);
            $table->string('ta3_nmode', 4);
            $table->string('ta3_model', 40);
            $table->string('ta3_codia', 7);
            $table->string('ta3_cgrup', 4);
            $table->string('ta3_creas', 7);
            $table->string('ta3_anioe', 4);
            $table->string('ta3_pre01', 6);
            $table->string('ta3_pre02', 6);
            $table->string('ta3_pre03', 6);
            $table->string('ta3_pre04', 6);
            $table->string('ta3_pre05', 6);
            $table->string('ta3_pre06', 6);
            $table->string('ta3_pre07', 6);
            $table->string('ta3_pre08', 6);
            $table->string('ta3_pre09', 6);
            $table->string('ta3_pre10', 6);
            $table->string('ta3_pre11', 6);
            $table->string('ta3_pre12', 6);
            $table->string('ta3_pre13', 6);
            $table->string('ta3_pre14', 6);
            $table->string('ta3_pre15', 6);
            $table->string('ta3_pre16', 6);
            $table->string('ta3_pre17', 6);
            $table->string('ta3_pre18', 6);
            $table->string('ta3_pre19', 6);
            $table->string('ta3_pre20', 6);
            $table->string('ta3_pre21', 6);
            $table->string('ta3_pre22', 6);
            $table->string('ta3_pre23', 6);
            $table->string('ta3_pre24', 6);
            $table->string('ta3_pre25', 6);
            $table->string('ta3_pre26', 6);
            $table->string('ta3_pre27', 6);
            $table->string('ta3_pre28', 6);
            $table->string('ta3_pre29', 6);
            $table->string('ta3_pre30', 6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('infoauto_tautos30');
    }
}
