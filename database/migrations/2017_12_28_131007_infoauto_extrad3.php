<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoautoExtrad3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infoauto_extrad3', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ex3_codia');
            $table->string('ex3_tapcu');
            $table->string('ex3_aelec');
            $table->string('ex3_cabor');
            $table->string('ex3_fxeno');
            $table->string('ex3_lalea');
            $table->string('ex3_tpano');
            $table->string('ex3_slluv');
            $table->string('ex3_screp');
            $table->string('ex3_ipneu');
            $table->string('ex3_vleva');
            $table->string('ex3_bluet');
            $table->string('ex3_aterm');
            $table->string('ex3_rflat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infoauto_extrad3');
    }
}
