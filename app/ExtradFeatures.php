<?php

namespace App;
use Illuminate\Database\Eloquent\Model as EloquentModel;


class ExtradFeatures extends EloquentModel
{
    protected $hidden = [];
    protected $fillable = ['id' , 'infoauto_feature_id','table_extrad','field_extrad'];
    public $timestamps = false;
    public $table = 'infoauto_extrad_features';
    public $primaryKey = 'id';

}
