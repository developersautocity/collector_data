<?php
namespace App\Console\Commands;
use Log;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom as Parser;
use App\InfoautoLog;

class LoggingProcess
{
    public static function register( $estado, $descripcion = '' )
    {
        if (env('APP_ENV') != 'local'):
            try
            {
                Log::info('Request to logging process...');
                
                $client = new Client();
                $response = $client->request('POST', env('URL_LOGGING_PROCESS'),
                [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'timeout'  => 40.0,
                    'json' => [
                        'procesoId'    => env('LOGGING_PROCESO_ID'),
                        'estado'        => $estado,
                        'descripcion'   => $descripcion,
                    ],
                ]);
                
                Log::info( 'Logging request code: ' . $response->getStatusCode() );
                
                InfoautoLog::create(array(
                    'process_status'        => $estado,
                    'process_description'   => $descripcion,
                    'created_at'            => date("Y-m-d H:i:s")
                ));
            }
            catch(RequestException $e)
            {
                Log::info( $e->getRequest() );
            }
        endif;
    }
}
