<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use log;
use MongoDB;
use Illuminate\Support\Facades\Session;

class UpdateMongo extends Command
{
    protected $signature = 'infoauto:update:mongodb';
    protected $description = 'Update mongo database';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            if(!LastProcess::get())
                return;

            $countModified = 0;
            $countInserted = 0 ;
            LoggingProcess::register('iniciado', 'update mongodb');
            $startTime = microtime(true);

            $versions = DB::table('infoauto_versions')
                ->select('id', 'codigo_infoauto', 'model_id', 'name')
                ->where('enabled', '=', 1)
                ->get()
                ->toArray();

            foreach ($versions as $oVersion) :
                $arrayNew = $this->getArrayComplete($oVersion);
            $this->save($arrayNew, $oVersion->codigo_infoauto, $countModified, $countInserted);
            endforeach;

            $timeElapsedSecs = microtime(true) - $startTime;
            LoggingProcess::register('finalizado', 'update mongodb: Registros modificados: ' . $countModified . ' - Registros insertados: ' . $countInserted);
            Log::info('Command successfully executed in ' . $timeElapsedSecs . ' seconds.');
        } catch (Exception $e) {
            LoggingProcess::register('error', "update mongodb: ".$e->getMessage());
            Log::error($e->getMessage());
            dd($e->getMessage());
        }
    }

    private function getArrayComplete($oVersion)
    {
        $arrData = DB::table('view_infoauto_mongo')
            ->where('codigo_infoauto', '=', $oVersion->codigo_infoauto)
            ->first();

        $arrPrecio = DB::table('infoauto_prices')
            ->select('year', 'amount')
            ->where('version_id', '=', $oVersion->id)
            ->orderby('year', 'asc')
            ->get()
            ->toArray();

        if (empty($arrPrecio)) :
            $arrPrecio  = array();
        $fecha_ultimoPrecio = '-'; else :
            foreach ($arrPrecio as $oprecio) :
                $fecha_ultimoPrecio = $oprecio->year;
        endforeach;
        endif;


        $posCero   =  '-';
        if (!empty($arrData)) :
            $posCero  = strripos($oVersion->codigo_infoauto, '0');
        $nMode    = substr($oVersion->codigo_infoauto, $posCero + 1);
        $arrData =  array(
                        'CODIA'  => $oVersion->codigo_infoauto ,
                        'NMARC'  => $arrData->infoauto_brand_id ,
                        'MARCA'  => $arrData->name ,
                        'NMODE'  => $nMode ,
                        'MODEL'  => $oVersion->name ,
                        'NGRUP'  =>  $arrData->model,
                        'CGRUP'  => $arrData->infoauto_model_id,
                        'LARGO'  => $arrData->ex2_largo ,
                        'ANCHO'  => $arrData->ex2_ancho ,
                        'ALTO'   => $arrData->ex2_alto ,
                        'ANIOE'  => $fecha_ultimoPrecio,
                        'COMBU'  => $arrData->ext_combu ,
                        'ALIME'  => $arrData->ext_alime ,
                        'MOTOR'  => $arrData->ext_motor,
                        'PUERT'  => $arrData->ext_puert,
                        'CLASI'  => $arrData->ext_clasi,
                        'CABIN'  => $arrData->ext_cabin,
                        'CARGA'  => $arrData->ext_carga == '0' ? true :  false,
                        'PESOT'  => $arrData->ext_pesot,
                        'VELOC'  => $arrData->ext_veloc,
                        'POTEN'  => $arrData->ext_poten,
                        'DIREC'  => $arrData->ext_direc,
                        'AIREA'  => $arrData->ext_airea == '0' ? true :  false,
                        'TRACC'  => $arrData->ext_tracc,
                        'CAJAV'  => $arrData->ext_cajav,
                        'FRABS'  => $arrData->ext_frabs == '0' ? true :  false,
                        'AIRBA'  => $arrData->ext_airba == '0' ? true :  false,
                        'CLIMA'  => $arrData->ex2_clima == '0' ? true :  false,
                        'SESTA'  => $arrData->ex2_sesta == '0' ? true :  false,
                        'FANTI'  => $arrData->ex2_fanti == '0' ? true :  false,
                        'TCORR'  => $arrData->ex2_tcorr == '0' ? true :  false,
                        'ALATE'  => $arrData->ex2_alate == '0' ? true :  false,
                        'ACABE'  => $arrData->ex2_acabe == '0' ? true :  false,
                        'ACORT'  => $arrData->ex2_acort == '0' ? true :  false,
                        'ARODI'  => $arrData->ex2_arodi == '0' ? true :  false,
                        'ISOFI'  => $arrData->ex2_isofi == '0' ? true :  false,
                        'CTRAC'  => $arrData->ex2_ctrac == '0' ? true :  false,
                        'CESTA'  => $arrData->ex2_cesta == '0' ? true :  false,
                        'CDESC'  => $arrData->ex2_cdesc == '0' ? true :  false,
                        'SAPEN'  => $arrData->ex2_sapen == '0' ? true :  false,
                        'BDIFE'  => $arrData->ex2_bdife == '0' ? true :  false,
                        'CDINA'  => $arrData->ex2_cdina == '0' ? true :  false,
                        'RELEF'  => $arrData->ex2_relef == '0' ? true :  false,
                        'AFREE'  => $arrData->ex2_afree == '0' ? true :  false,
                        'TAPCU'  => $arrData->ex3_tapcu == '0' ? true :  false,
                        'ATERM'  => $arrData->ex3_aterm == '0' ? true :  false,
                        'AELEC'  => $arrData->ex3_aelec == '0' ? true :  false,
                        'CABOR'  => $arrData->ex3_cabor == '0' ? true :  false,
                        'TPANO'  => $arrData->ex3_tpano == '0' ? true :  false,
                        'SLLUV'  => $arrData->ex3_slluv == '0' ? true :  false,
                        'SCREP'  => $arrData->ex3_screp == '0' ? true :  false,
                        'IPNEU'  => $arrData->ex3_ipneu == '0' ? true :  false,
                        'VLEVA'  => $arrData->ex3_vleva == '0' ? true :  false,
                        'BLUET'  => $arrData->ex3_bluet == '0' ? true :  false,
                        'RFLAT'  => $arrData->ex3_rflat == '0' ? true :  false,
                        'PRICES' => $arrPrecio,
                        'updatedAt'=>  date('Y-m-d\TH:i:s')
                    );
        endif;

        return $arrData;
    }

    private function save($arrData, $codia, &$countModified, &$countInserted)
    {
        if (env('APP_ENV') != 'local') :
            $connection = new MongoDB\Client("mongodb://{$_ENV['INFOAUTO_MONGO_USERNAME']}:{$_ENV['INFOAUTO_MONGO_PASSWORD']}@{$_ENV['INFOAUTO_MONGO_URL_CONEXION']}"); else:
            $connection = new MongoDB\Client("mongodb://{$_ENV['INFOAUTO_MONGO_URL_CONEXION']}");
        endif;

        $collection = $connection->{$_ENV['INFOAUTO_MONGO_DB']}
            ->{$_ENV['INFOAUTO_MONGO_TABLE']};

        $cursor = $collection->findOne(['CODIA' => $codia]);

        if (is_null($cursor)) :
            $arrDataData = ['createdAt'=>  date('Y-m-d\TH:i:s')];
        $arrMerge = array_merge($arrData, $arrDataData);
        $result = $collection->insertOne($arrMerge);
        $countInserted ++; else:
            $countModified  ++;
        $result = $collection->updateOne(
            [ 'CODIA'   => $codia ],
            [ '$set'    =>  $arrData ],
            [ '$upsert' => true]
        );
        endif;

        return  $result ;
    }
}
