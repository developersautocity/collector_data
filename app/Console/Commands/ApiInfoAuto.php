<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use XBase\Table;
use App\Brand;
use App\Model;
use App\Version;
use App\Price;
use Exception;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom as Parser;
use Illuminate\Support\Facades\Session;

class ApiInfoAuto
{
    public function getToken()
    {
        $client = new Client();

        $url = $_ENV['INFOAUTO_URL_API']."auth/login";
        $authorization = base64_encode($_ENV['INFOAUTO_USERNAME'] . ":" . $_ENV['INFOAUTO_PASSWORD']);
        
        $response = $client->request('POST', $url ,
        [
            'headers' => [
                'Content-Type'      => "application/json",
                'Content-Length'    => "0",
                'Authorization'     => "Basic {$authorization}"
            ],
            'timeout'  => 40.0,
        ]);
        
        if($response->getStatusCode() != 200):
            throw new Exception('Error to get Token. Code: '. $response->getStatusCode(). '. Message: ' . $response->getBody()->getContents());
        endif;

        $data = json_decode($response->getBody());

        $accessToken = $data->access_token;
        Session::put('access_token', $accessToken);

        return $accessToken;
    }

    public function refreshToken()
    {
        $refreshToken =  Session::get('refresh_token');
        $headers = array(
            "Content-type: application/json",
            "Authorization: Bearer " .   $refreshToken
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  $_ENV['INFOAUTO_URL_API']."auth/refresh");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        $response =json_decode($response);

        Session::put('access_token', $response->access_token);
        Session::put('tokenTiempoApertura', time());

        return $response->access_token;
    }

    public function getFechaActualizacionlocal($token)
    {
        $headers = array(
            "Content-type: application/json",
            "Authorization: Bearer " . $token
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,  $_ENV['INFOAUTO_URL_API']."pub/brands/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    public function getCodiasLocal(int $limit=10,  $fechaAutocity)
    {
        $versions = DB::table('infoauto_versions')
            ->select('id', 'codigo_infoauto')
            ->where('enabled', '=', 1)
            ->whereDate('date_update', '<', date( $fechaAutocity))
            ->orWhereNull('date_update')
            ->where('enabled', '=', 1)
            ->get()
            ->unique('id')
            ->take($limit)
            ->toArray();

        return $versions;
    }

    public function getArrCodiaDiff($arrCodiasInfoAuto, $arrCodiaLocales)
    {
        $arrGetCodiaInfoAuto = array_map(function ($object) {
            return $object->codia;
        }, $arrCodiasInfoAuto);

        $arrGetCodiaLocal = array_map(function ($object) {
            return $object->codigo_infoauto;
        },  $arrCodiaLocales);

        return array_diff( $arrGetCodiaInfoAuto, $arrGetCodiaLocal );
    }

    public function getCodiasInfoAuto()
    {
        $client = new Client();

        $accesToken = $this->getToken();
        $url = $_ENV['INFOAUTO_URL_API']."pub/download";

        $response = $client->request('GET', $url ,
        [
            'headers' => [
                'Content-Type'      => "application/json",
                'Accept-Encoding'   => "gzip",
                'Authorization'     => "Bearer {$accesToken}",
            ],
            'timeout'  => 40.0,
            'http_errors' => false,
        ]);

        return  $response;
    }

    public function getBrands($token)
    {
        $headers = array(
            "Content-type: application/json",
            "Authorization: Bearer " . $token
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  $_ENV['INFOAUTO_URL_API']."pub/brands/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    public function getAllBrands()
    {
        $accesToken = $this->getToken();
        $client = new Client();

        $url = $_ENV['INFOAUTO_URL_API']."pub/brands/download";

        $response = $client->request('GET', $url ,
        [
            'headers' => [
                'Content-Type'      => "application/json",
                'Accept-Encoding'   => "gzip",
                'Authorization'     => "Bearer {$accesToken}"
            ],
            'timeout'  => 40.0,
        ]);

        return $response;
    }

    public function getAllModelsByBrand( $infoauto_brand_id, $pageSize = 100, $pageNumber = 1)
    {
        $accesToken = $this->getToken();
        $client = new Client();

        $url = $_ENV['INFOAUTO_URL_API']."pub/brands/{$infoauto_brand_id}/models?page={$pageNumber}&page_size={$pageSize}";

        $response = $client->request('GET', $url ,
        [
            'headers' => [
                'Content-Type'      => "application/json",
                'Authorization'     => "Bearer {$accesToken}"
            ],
            'timeout'  => 40.0,
        ]);

        return $response;
    }

    public static function sleepInfoAuto()
    {
        \sleep($_ENV['INFOAUTO_SLEEP']);
    }

    public function getCurrentYear()
    {
        $accesToken = $this->getToken();
        $client = new Client();

        $url = $_ENV['INFOAUTO_URL_API']."pub/current_year";

        $response = $client->request('GET', $url ,
        [
            'headers' => [
                'Content-Type'      => "application/json",
                'Authorization'     => "Bearer {$accesToken}"
            ],
            'timeout'  => 40.0,
        ]);

        return $response;
    }

    function getModelByBrandGroup($infoautoBrandID, $infoautoGroupID)
    {
        $model = DB::table('infoauto_brands')
            ->select('infoauto_models.id')
            ->leftJoin('infoauto_models','infoauto_brands.id','=','infoauto_models.brand_id')
            ->where('infoauto_brands.infoauto_brand_id', '=', $infoautoBrandID)
            ->where('infoauto_models.infoauto_model_id', '=', $infoautoGroupID)
            ->first();

        return $model;
    }
}
