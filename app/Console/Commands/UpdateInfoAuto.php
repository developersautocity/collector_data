<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use XBase\Table;
use App\Brand;
use App\Model;
use App\Version;
use App\Price;
use App\LastUpdateDateApi;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom as Parser;
use Illuminate\Support\Facades\Session;
use App\Console\Commands\ApiInfoAuto;
use log;
use App\ExtradFeatures;
use App\Extrad;
use App\Extrad2;
use App\Extrad3;
use App\Extrad4;
class UpdateInfoAuto extends Command
{
    protected $signature = 'infoauto:update';
    protected $description = 'Update Infoauto database';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try
        {
            if(!LastProcess::get())
                return;

            LoggingProcess::register( 'iniciado', 'Update Infoauto database' );
            $ApiInfoAuto = new ApiInfoAuto();
            $startTime = microtime(true);

            $oLastDateUpdate = $this->getLastDate();

            if(empty($oLastDateUpdate[0]->last_date_update))
                throw new Exception("Error to get last date update");

            $lastDateUpdate = $oLastDateUpdate[0]->last_date_update;

            $responseCurrenyYear = $ApiInfoAuto->getCurrentYear();
            if($responseCurrenyYear->getStatusCode() != 200):
                throw new Exception('Error to get current year by infoauto API. Code: '. $statusCode. '. Message: ' . $responseCurrenyYear->getBody()->getContents());
            endif;
            $bodyCurrentYear = \json_decode($responseCurrenyYear->getBody());

            if(empty($bodyCurrentYear->year)):
                throw new Exception('Error. Message: Error to get current year' );
            endif;
            $yearInfoAuto = $bodyCurrentYear->year;

            $this->updateOrCreateCodias($lastDateUpdate, $yearInfoAuto);

            $timeElapsedSecs = microtime(true) - $startTime;
            LoggingProcess::register( 'finalizado', 'Process Update Infoauto database successfully finished' );
            Log::info('Command successfully executed in ' . $timeElapsedSecs . ' seconds.');
        }
        catch(Exception $e)
        {
            LoggingProcess::register( 'error', "Process Update Infoauto database: " . $e->getMessage() );
            Log::error($e->getMessage());
        }
    }

    private function getLastDate()
    {
        $oLastDateUpdate = DB::table('infoauto_last_update')
                ->orderBy('id' , 'desc')
                ->take(1)
                ->get();

        return $oLastDateUpdate;
    }

    private function updateOrCreateCodias($lastDateUpdate, $yearInfoAuto)
    {
        $ApiInfoAuto = new ApiInfoAuto();

        $response = $ApiInfoAuto->getCodiasInfoAuto();
        
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200):
            throw new Exception('Error code: '. $statusCode. '. Message: ' . $response->getBody()->getContents());
        endif;

        $codiasInfoAuto  = \json_decode($response->getBody());
        
        foreach($codiasInfoAuto as $xCodiaInfoAuto ):
            try{
                DB::beginTransaction();

                $oVersion = Version::firstOrNew(
                    array('codigo_infoauto' => $xCodiaInfoAuto->codia)
                );

                if(empty($oVersion->id)):
                    /* Create new version */
                    $oModel = $ApiInfoAuto->getModelByBrandGroup($xCodiaInfoAuto->brand->id, $xCodiaInfoAuto->group->id);
                    
                    if(empty($oModel->id)):
                        continue;
                    endif;
                    
                    $oVersion->model_id         = $oModel->id;
                    $oVersion->codigo_infoauto  = $xCodiaInfoAuto->codia;
                    $oVersion->name             = $xCodiaInfoAuto->description;
                    $oVersion->enabled          = true;
                    $oVersion->created_at       = date("Y-m-d H:i:s");

                    $oVersion->save();
                endif;
                
                $versionID = $oVersion->id;
                if(empty($versionID)):
                    continue;
                endif;

                if(is_null($oVersion->date_update) || $oVersion->date_update < $lastDateUpdate):
                    /* Prices last years */
                    if(!empty($xCodiaInfoAuto->prices)):
                        $arrYears = array();

                        foreach($xCodiaInfoAuto->prices as $xPrice):
                            array_push($arrYears, $xPrice->year);

                            Price::updateOrCreate(
                                ['version_id' => $versionID, 'year' => $xPrice->year],
                                ['amount' => $xPrice->price ]
                            );
                        endforeach;

                        /* Delete year not updated */
                        if(!empty($versionID)):
                            $count = Price::where('version_id', $versionID)->count();

                            if($count != count($arrYears)):
                                Price::where('version_id', $versionID)
                                    ->whereNotIn('year', $arrYears)
                                    ->delete();
                            endif;
                        endif;
                    endif;
                    
                    /* Prices current year */
                    if($xCodiaInfoAuto->list_price != null):
                        Price::updateOrCreate(
                            ['version_id' => $versionID, 'year' => $yearInfoAuto],
                            ['amount' => $xCodiaInfoAuto->list_price ]
                        );
                    endif;

                    /* Features */
                    $this->updateFeatures($xCodiaInfoAuto);

                    $oVersion->date_update    = $lastDateUpdate;
                    $oVersion->updated_at     = date("Y-m-d H:i:s");
                    $oVersion->save();
                endif;
            
                DB::commit();
            }
            catch(Exception $ex)
            {
                DB::rollback();
                Log::error($ex->getMessage());
            }
        endforeach;
    }

    private function updateFeatures($xCodiaInfoAuto)
    {
        if(!empty($xCodiaInfoAuto)):
            foreach($xCodiaInfoAuto->features as $xFeature):
                $oExtradFeatures = ExtradFeatures::Where('infoauto_feature_id', $xFeature->id)->first();

                if (!empty( $oExtradFeatures )) :
                    switch ( $oExtradFeatures->table_extrad ) :
                        case 'infoauto_extrad':
                            $this->updateExtrad($xCodiaInfoAuto->codia, $oExtradFeatures, $xFeature);
                            break;
                        case 'infoauto_extrad2':
                            $this->updateExtrad2($xCodiaInfoAuto->codia, $oExtradFeatures, $xFeature);
                            break;
                        case 'infoauto_extrad3':
                            $this->updateExtrad3( $xCodiaInfoAuto->codia, $oExtradFeatures, $xFeature);
                            break;
                        case 'infoauto_extrad4':
                            $this->updateExtrad4( $xCodiaInfoAuto->codia, $oExtradFeatures, $xFeature);
                            break;
                        default:
                            break;
                    endswitch;
                endif;
            endforeach;
        endif;
    }

    private function updateExtrad($codia, $oExtradFeatures, $xFeature)
    {
        switch ($oExtradFeatures->field_extrad) :
            case 'ext_cabin':
                $value = ((!empty($xFeature->value)) ? $xFeature->value : '.');
                if ($value == '') :
                    $value = '.' ;
                endif;
                break;

            case 'ext_impor':
                $value = ((!empty($xFeature->value)) ? $xFeature->value : 'NO');
                break;
            case 'ext_carga':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'S' :  'N');
                break;
            case 'ext_frabs':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ext_airba':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ext_airea':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            default:
                $value = ((!empty($xFeature->value)) ? $xFeature->value : '');
                break;
        endswitch ;

        Extrad::updateOrCreate(
            ['ext_codia' => $codia  ],
            [$oExtradFeatures->field_extrad  => $value]
        );
    }

    private function updateExtrad2(int $codia,$oExtradFeatures,$xFeature)
    {
        switch ($oExtradFeatures->field_extrad) :
            case 'ex2_fanti':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_tcorr':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_alate':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_acabe':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_acort':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_arodi':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_isofi':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_ctrac':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_cdesc':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_sapen':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_cdina':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_bdife':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_relef':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_afree':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_rparf':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'ex2_cesta':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            default:
                $value = ((!empty($xFeature->value)) ? $xFeature->value : '');
                break;
        endswitch ;

        Extrad2::updateOrCreate(
            ['ex2_codia' => $codia ],
            [ $oExtradFeatures->field_extrad  => $value]
        );
    }

    private function updateExtrad3(int $codia,$oExtradFeatures,$xFeature)
    {
        switch ($oExtradFeatures->field_extrad) :
            case 'ex3_aelec':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_tapcu':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_cabor':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_lalea':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_slluv':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_screp':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_ipneu':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_vleva':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_bluet':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_aterm':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            case  'ex3_rflat':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value        =  (($featureValue) ? 'SI' :  'NO');
                break;
            default:
                $value        = ((!empty($xFeature->value)) ? $xFeature->value : '');
                break;
        endswitch ;

        Extrad3::updateOrCreate(
            ['ex3_codia' => $codia ],
            [ $oExtradFeatures->field_extrad  => $value]
        );
    }

    private function updateExtrad4(int $codia,$oExtradFeatures,$xFeature)
    {
        switch ($oExtradFeatures->field_extrad) :
            case 'control_estabilidad':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'llantas_alineacion':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'alarma_luces':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'asistente_estacionamiento':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'boton_arranque':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'cierre_centralizado_a_distancia':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'conectividad':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'navegador_satelital':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'pantalla':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'start_stop':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'advertencia_colision':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'advertencia_salida_carril':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'alerta_cansancio':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'bloqueo_puertas':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'control_frenado_curvas':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'control_velocidad_crucero':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'espejo_interior_fotocromatico':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'sistema_mantenimiento_carril':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            case 'limitador_velocidad':
                $featureValue = ((!empty($xFeature->value)) ? $xFeature->value : 0);
                $value =  (($featureValue) ? 'SI' :  'NO');
                break;
            default:
                $value = ((!empty($xFeature->value)) ? $xFeature->value : '');
                break;
        endswitch ;

        Extrad4::updateOrCreate(
            ['codia' => $codia ],
            [ $oExtradFeatures->field_extrad  => $value]
        );
    }
}
