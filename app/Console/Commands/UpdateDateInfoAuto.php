<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use XBase\Table;
use App\Brand;
use App\Model;
use App\Version;
use App\Price;
use App\LastUpdateDateApi;
use Exception;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom as Parser;
use Log;

class UpdateDateInfoAuto extends Command
{
    protected $signature = 'infoauto:getDate';
    protected $description = 'Parses Infoauto database';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            LoggingProcess::register( 'iniciado', 'Get last date update Infoauto' );

            $ApiInfoAuto = new ApiInfoAuto();
            $token = $ApiInfoAuto->getToken();

            $fechaAutocity = $this->getDateTimeInfoAuto($token);

            LoggingProcess::register( 'finalizado', 'Last date update infoauto successfully' );
        }
        catch(Exception $e)
        {
            LoggingProcess::register( 'error', $e->getMessage() );
            Log::error($e->getMessage());
        }
    }

    public function getDateTimeInfoAuto($token)
    {
        $headers = array(
            "Content-type: application/json",
            "Authorization: Bearer " . $token
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  $_ENV['INFOAUTO_URL_API']."pub/datetime");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);
        $fechaAutocity =  json_decode($response);
        
        return $this->updateDate($fechaAutocity->updated_at);
    }

    private function updateDate($updated_at)
    {
        try
        {
            $lastDateUpdate = new LastUpdateDateApi();
        
            $updated_at = substr($updated_at,0,10) ;
            $fechaLocal = DB::table('infoauto_last_update')->select('last_date_update')
                ->where('last_date_update', '=', $updated_at )
                ->get()
                ->toArray();

            if(empty($fechaLocal) ) :
                $lastDateUpdate->last_date_update= $updated_at;
                $lastDateUpdate->save();
            endif;

            return $updated_at;
        }
        catch(Exception $e)
        {
           LoggingProcess::register( 'error', $e->getMessage() );
        }
    }

}
