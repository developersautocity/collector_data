<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use XBase\Table;
use App\Brand;
use App\Group;
use App\Model;
use App\Version;
use App\Price;
use App\InfoautoLog;
use App\LastUpdateDateApi;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom as Parser;
use Illuminate\Support\Facades\Session;
use App\Console\Commands\ApiInfoAuto;
use log;

class CreateBrandsGroupsInfoAuto extends Command
{
    protected $signature = 'infoauto:create';
    protected $description = 'Create Infoauto database';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try
        {
            if(!LastProcess::get())
                return;
            
            $ApiInfoAuto = new ApiInfoAuto();
            LoggingProcess::register( 'iniciado', 'Create new Brands and Groups' );

            $startTime = microtime(true);

            $fechaAutocity= DB::table('infoauto_last_update')
                ->orderBy('id' , 'desc')
                ->take(1)
                ->get();

            if(empty($fechaAutocity[0]->last_date_update)) :
                throw new Exception("Error to get last date update");
            endif;

            $fechaAutocity = $fechaAutocity[0]->last_date_update;
            $response = $ApiInfoAuto->getAllBrands();

            if($response->getStatusCode() != 200):
                throw new Exception('Error to get Brands. Code: '. $statusCode. '. Message: ' . $response->getBody()->getContents());
            endif;

            $brands = \json_decode($response->getBody());

            if (!empty($brands)):
                foreach ($brands as $key => $xBrand):
                    $brand = Brand::firstOrNew(['infoauto_brand_id' => $xBrand->id]);

                    if(empty($brand->id)):
                        $brand->name = $xBrand->name;
                        $brand->created_at = date('Y-m-d H:i:s');
                        $brand->save();
                    endif;
                    
                    $xBrand->brand_id_autocity = $brand->id;
                    $this->createGroup($xBrand);
                endforeach;
            endif;

            $timeElapsedSecs = microtime(true) - $startTime;

            LoggingProcess::register( 'finalizado', 'Process create brands and groups successfully' );
            Log::info('Command successfully executed in ' . $timeElapsedSecs . ' seconds.');
        }
        catch(Exception $e)
        {
            LoggingProcess::register( 'error', "Process create: ".$e->getMessage() );
            Log::error($e->getMessage());
            dd($e->getMessage());
        }
    }

    private function createGroup($xBrand)
    {
        if (!empty($xBrand)):
            foreach ($xBrand->groups as $key => $xGroup):
                /* Create Group */
                $oGroup = Group::firstOrNew([
                    'gru_nmarc' => $xBrand->id,
                    'gru_cgrup' => $xGroup->id
                ]);

                if(empty($oGroup->id)):
                    $oGroup->gru_ngrup = $xGroup->name;
                    $oGroup->save();
                endif;

                /* Create Model */
                $oModel = Model::firstOrNew([
                    'brand_id'          => $xBrand->brand_id_autocity,
                    'infoauto_model_id' => $xGroup->id
                ]);
    
                if(empty($oModel->id)):
                    $oModel->name = $xGroup->name;
                    $oModel->save();
                endif;
            endforeach;
        endif;
    }
}
