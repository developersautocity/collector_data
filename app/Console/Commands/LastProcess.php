<?php
namespace App\Console\Commands;
use Log;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom as Parser;
use App\InfoautoLog;

class LastProcess
{
    public static function get()
    {
        $process_status = InfoautoLog::select('process_status')->orderBy('id', 'desc')->first();
            
        if(empty($process_status['process_status']))
            return false;

        if($process_status['process_status'] == "finalizado")
            return true;

        return false;
    }
}
