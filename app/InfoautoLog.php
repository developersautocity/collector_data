<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class InfoautoLog extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_log';
    protected $primaryKey = 'id';
    protected $fillable = ['id','process_status','process_description','created_at'];
    protected $hidden = [];
}
