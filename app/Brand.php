<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;
class Brand extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_brands';
    protected $primaryKey = 'id';
    protected $fillable = ['id','infoauto_brand_id','name'];
    protected $hidden = [];
}
