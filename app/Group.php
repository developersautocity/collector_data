<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Group extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_grupos';
    protected $fillable = ['id','gru_nmarc','gru_cgrup','gru_ngrup'];
    protected $hidden = [];
}
