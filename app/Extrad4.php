<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Extrad4 extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_extrad4';
    protected $primaryKey = 'id';
    protected $fillable = ['id','codia','control_estabilidad','llantas_alineacion','alarma_luces','asistente_estacionamiento','boton_arranque','cierre_centralizado_a_distancia','espejo_exterior_electrico','levanta_vidrio_electrico','conectividad','navegador_satelital','pantalla','start_stop','apertura_porton','frenos_delanteros','frenos_traseros','relacion_peso_potencia','autonomia','capacidad_tanque','aceleracion_0_100','consumo_ciudad','consumo_promedio','consumo_ruta','advertencia_colision','advertencia_salida_carril','alerta_cansancio','bloqueo_puertas','control_frenado_curvas','control_velocidad_crucero','espejo_interior_fotocromatico','sistema_mantenimiento_carril','opticas_delanteras','capacidad_baul','opticas_traseras','limitador_velocidad'];
    protected $hidden = [];


}
