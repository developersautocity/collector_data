<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Extrad extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_extrad';
    protected $primaryKey = 'id';
    protected $fillable = ['id','ext_codia','ext_combu','ext_alime','ext_motor','ext_puert','ext_clasi','ext_cabin','ext_carga','ext_pesot','ext_veloc','ext_poten','ext_direc','ext_airea','ext_tracc','ext_impor','ext_cajav','ext_frabs','ext_airba'];
    protected $hidden = [];


}
