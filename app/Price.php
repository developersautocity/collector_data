<?php

namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use App\Version;

class Price extends EloquentModel
{
    protected $hidden = ['id', 'version_id'];
    protected $fillable = ['id','version_id','year','amount'];

    public $timestamps = false;
    public $table = 'infoauto_prices';

    public function version()
    {
        return $this->belongsTo(Version::class);
    }
}
