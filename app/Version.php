<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use App\Model;
use App\Price;
use App\Data;
class Version extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_versions';
    protected $fillable = ['id','model_id','codigo_infoauto','codigo_reasignado','name','date_update','enabled','created_at'];
    protected $hidden = [];

    public function model()
    {
        return $this->belongsTo(Model::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function data()
    {
        return $this->hasOne(Data::class, 'id');
    }
}
