<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Extrad2 extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_extrad2';
    protected $primaryKey = 'id';
    protected $fillable = ['id','ex2_codia','ex2_clima','ex2_fanti','ex2_tcorr','ex2_sesta','ex2_alate','ex2_acabe','ex2_acort','ex2_arodi','ex2_isofi','ex2_ctrac','ex2_cesta','ex2_cdesc','ex2_sapen','ex2_cdina','ex2_bdife','ex2_relef','ex2_afree','ex2_rparf','ex2_largo','ex2_ancho','ex2_alto'];
    protected $hidden = [];


}
