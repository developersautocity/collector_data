<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use App\Brand;
class Model extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_models';
    protected $primaryKey = 'id';
    protected $fillable = ['id','brand_id','infoauto_model_id','name'];
    protected $hidden = [];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
