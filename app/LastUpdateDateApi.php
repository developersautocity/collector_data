<?php

namespace App;
use Illuminate\Database\Eloquent\Model as EloquentModel;


class LastUpdateDateApi extends EloquentModel
{
    protected $hidden = [];
    protected $fillable = ['id' , 'last_date_update'];
    public $timestamps = false;
    public $table = 'infoauto_last_update';
    public $primaryKey = 'id';

}