<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class Data extends EloquentModel
{
    protected $table = 'infoauto_extrad';

    public $timestamps = false;

    public function version()
    {
        return $this->belongsTo('App\Model\Data', 'ext_codia', 'id');
    }

    public function getTypeAttribute()
    {
        $namesArray = [
            'CUP'=> 'Cupe',
            'CAB'=> 'Cabriolet',
            'SED'=> 'Sedan',
            'FUA'=> 'Furgones chicos (Courrier)',
            'FUB'=> 'Furgones grandes (Trafic)',
            'JEE'=> 'Jeep',
            'MBU'=> 'Minibús',
            'RUR'=> 'Rural',
            'MKA'=> 'Pick-Up carga menos de 1000kg',
            'PKB'=> 'Pick-Up carga mas de 1000kg',
            'PB4'=> 'Pick-Up carga mas de 1000kg 4x4',
            'VAN'=> 'Familiares (Caravan)',
            'WAG'=> 'Camionetas familiares (Cherokee)',
            'WA4'=> 'Camionetas Familiares 4x4',
            'LIV'=> 'Camiones livianos',
            'SPE'=> 'Camiones semi-pesados',
            'PES'=> 'Camiones pesados',
            'COL'=> 'Colectivos',
            'MIV'=> 'Minivan (Meriva)'
        ];

        return $namesArray[$this->ext_clasi];
    }
}