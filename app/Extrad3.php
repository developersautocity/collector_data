<?php
namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Extrad3 extends EloquentModel
{
    public $timestamps = false;
    public $table = 'infoauto_extrad3';
    protected $primaryKey = 'id';
    protected $fillable = ['id','ex3_codia','ex3_tapcu','ex3_aelec','ex3_cabor','ex3_fxeno','ex3_lalea','ex3_tpano','ex3_slluv','ex3_screp','ex3_ipneu','ex3_vleva','ex3_bluet','ex3_aterm','ex3_rflat'];
    protected $hidden = [];


}
